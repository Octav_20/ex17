package ro.orangetraining;
import java.io.*;
import java.util.*;


public class Main {

    public static void main(String[] args) throws IOException {

        /* Am incercat sa generalizez problema astfel incat valorile pentru dimensiunea vectorului si
       * valorile elementelor sa fie introduse de la tastatura*/
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce the number of elements n= ");
        int number = scanner.nextInt();
        //Initializam vectorul V unde vom stoca valorile elementelor;
        ArrayList<Integer> V = new ArrayList<>();
        //Citim de la tastatura si salvam in vector;
        for (int j = 0; j < number; j++) {
            System.out.print("V[" + j + "]: ");
            V.add(scanner.nextInt());
        }
        //Afisam vectorul
        for (int j = 0; j < number; j++) System.out.println(V.get(j)+" ");

    }

}

